# There are six different peg colors.
# A hash containing the colors has been provided in the code. The colors are:
# Red
# Green
# Blue
# Yellow
# Orange
# Purple
# The computer will generate a random code of four pegs.
# (Duplicate colors are allowed.)
# The user gets ten turns to guess the code.
# You decide how the user inputs his guess
# (maybe like so: "RGBY" for red-green-blue-yellow)
# The computer should tell the player how many
# exact matches (right color in right spot)
# and near matches (right color, wrong spot) he or she has.
# The game ends when the user guesses the code, or is out of turns.

# represent a sequence of four pegs
# make code objects for both the secret code and the user's guess
class Code
  attr_reader :pegs

  PEGS = { "R" => "red", "O" => "orange", "Y" => "yellow",
    "G" => "green", "P" => "purple", "B" => "blue" }

  def initialize(pegs)
    @pegs = pegs
  end

  # builds a Code instance with random peg colors
  def self.random
    pegs = []
    4.times { pegs << PEGS[PEGS.keys.sample] }

    @answer_code = Code.new(pegs)
  end

  # takes a user input string like "RGBY" and builds a Code object
  def self.parse(input)
    user_guess = input.upcase.split("")
    pegs = []
    user_guess.each do |peg|
      raise "Invalid peg" if PEGS[peg] == nil
      pegs << PEGS[peg]
    end

    @guessed_code = Code.new(pegs)
  end

  # index into Code#pegs
  def [](peg_pos)
    @pegs[peg_pos]
  end

  # returns true when passed a matching Code
  def ==(other_code)
    # check that a Code object was passed
    return false unless other_code.instance_of? Code

    if exact_matches(other_code) == self.pegs.length
      true
    else
      false
    end
  end

  # handle exact match comparisons
  def exact_matches(other_code)
    correct_matches = 0

    (0...other_code.pegs.length).each do |index|
      if other_code[index] == self[index]
        correct_matches += 1
      end
    end

    correct_matches
  end

  # handle near match comparisons
  def near_matches(other_code)
    all_matches = 0
    counter = Hash.new(0)

    # create counter hash of colors in other_code
    other_code.pegs.each do |el|
      counter[el] += 1
    end

    # add up minimum # of matching colors
    counter.each do |key, value|
      all_matches += value < self.pegs.count(key)? value : self.pegs.count(key)
    end

    # calculate and return near matches
    all_matches - exact_matches(other_code)
  end

end

# keep track of how many turns has passed, the correct Code,
# and prompt user for input
class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  # parse input and return Code object
  def get_guess

    ARGV.clear
    puts "Please provide a guess, e.g. RGBY"
    guess = gets.chomp
    Code.parse(guess)

  end

  def display_matches(code)
    puts "exact matches: " + @secret_code.exact_matches(code).to_s
    puts "near matches: " + @secret_code.near_matches(code).to_s
  end

  def play
    won = false
    turns = 0
    while turns <= 10
      guess = get_guess
      if guess == @secret_code
        puts "You won!"
        won = true
        break
      else
        display_matches(guess)
        puts "Try again!" unless turns == 10 
      end
      turns += 1
    end

    puts "Aww, you lost :(" if won == false
  end

end

if $PROGRAM_NAME == __FILE__
  Game.new(Code.parse("BRGY")).play
end
